tickets:
	echo "python3 src/tickets.py" > tickets
	chmod 777 tickets
sky_links:
	echo "python3 src/sky_links.py" > sky_links
	chmod 777 sky_links
holidays:
	echo "python3 src/holidays.py" > holidays
	chmod 777 holidays
wander:
	echo "python3 src/wander.py" > wander
	chmod 777 wander
company:
	echo "python3 src/company.py" > company
	chmod 777 company
communication:
	echo "python3 src/communication.py" > communication
	chmod 777 communication
distribution:
	echo "python3 src/distribution.py" > distribution
	chmod 777 distribution
travel:
	echo "python3 src/travel.py" > travel
	chmod 777 travel
