from graph import Graph

graph = Graph()
graph.create_elements_by_std_input()
graph.print_edges_by_edge_value_gte(2)