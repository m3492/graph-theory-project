class Node:

    def __init__(self):
        self.node_id = 0
        self.node_value = "undefined"
        self.input_edges = []
        self.output_edges = []

    def __init__(self, node_id, node_value):
        self.node_id = node_id
        self.node_value = node_value
        self.input_edges = []
        self.output_edges = []

    def __repr__(self):
        return "{ " + str(self.node_id) + ", " + str(self.node_value) + " }"

    def get_out_degree_including_edges_values(self):
        out_degree = 0
        for edge in self.output_edges:
            out_degree += edge.edge_value
        return out_degree

    def get_in_degree_including_edges_values(self):
        in_degree = 0
        for edge in self.input_edges:
            in_degree += edge.edge_value
        return in_degree
