import sys

from node import Node
from edge import Edge


class Graph:

    def __init__(self):
        self.name = "Graph"
        self.max_nodes_id = 0
        self.nodes = []
        self.max_edges_id = 0
        self.edges = []
        self.node_values_case_sensitivity = True
        self.num_of_nodes_rows = 1

    def create_elements_by_std_input(self):
        line_index = 0

        for input_line in sys.stdin:

            input_line = input_line.replace("\n", "")
            # create nodes
            if line_index < self.num_of_nodes_rows:
                self.create_nodes(input_line)
            # create edges
            else:
                self.create_edges_by_input_line(input_line)

            line_index += 1

    def create_nodes(self, input_line):
        if ": " in input_line:
            nodes_line = input_line.split(": ")
            # set graph name
            self.name = nodes_line[0]
            node_values = nodes_line[1].split(", ")
        else:
            node_values = input_line.split(", ")

        # create graph's nodes
        for node_value in node_values:
            node = self.get_node_by_value(node_value)
            if not node:
                self.max_nodes_id += 1
                node = Node(self.max_nodes_id, node_value)
                self.nodes.append(node)
        return self.nodes

    def create_edge(self, input_node: Node, output_node: Node):
        edge = self.get_edge_between_nodes(input_node, output_node)
        optional_reverse_edge = self.get_edge_between_nodes(output_node, input_node)
        if not edge:
            self.max_edges_id += 1
            edge = Edge(self.max_edges_id, 1, input_node, output_node)
            input_node.output_edges.append(edge)
            output_node.input_edges.append(edge)
            self.edges.append(edge)
        else:
            edge.edge_value += 1
        if optional_reverse_edge:
            edge.exists_reverse_edge = True
            optional_reverse_edge.exists_reverse_edge = True

    def create_bi_direct_edge(self, input_node: Node, output_node: Node):
        self.create_edge(input_node, output_node)
        self.create_edge(output_node, input_node)

    def create_edges_by_input_line(self, input_line):
        if ": " in input_line:
            input_line = input_line.split(": ")[1]

        if " -> " in input_line:
            input_edges_connector = " -> "
        elif " - " in input_line:
            input_edges_connector = " - "
        else:
            input_edges_connector = ", "

        input_e_nodes_values = input_line.split(input_edges_connector)
        e_nodes = []

        for i, input_edge_node in enumerate(input_e_nodes_values):
            if i >= len(input_e_nodes_values) - 1:
                break
            input_e_source_value = input_e_nodes_values[i].strip()
            input_e_dest_value = input_e_nodes_values[i + 1].strip()
            # get source node by node_value
            input_node = self.get_node_by_value(input_e_source_value)
            # get destination node by node_value
            output_node = self.get_node_by_value(input_e_dest_value)
            e_nodes.append(input_node)
            e_nodes.append(output_node)
            # create direct edge
            if input_edges_connector.__eq__(" -> "):
                self.create_edge(input_node, output_node)
            # create bi-direct edge
            elif input_edges_connector.__eq__(" - "):
                self.create_bi_direct_edge(input_node, output_node)
            else:
                self.create_bi_direct_edge(input_node, output_node)
                for idx in range(0, i):
                    self.create_bi_direct_edge(e_nodes[idx], output_node)

    def get_node_by_value(self, node_value):
        for node in self.nodes:
            if not self.node_values_case_sensitivity and isinstance(node_value, str):
                if node.node_value.upper() == node_value.upper():
                    return node
            elif node.node_value == node_value:
                return node
        return False

    def get_edge_between_nodes(self, start_node: Node, end_node: Node):
        for out_edge in start_node.output_edges:
            if out_edge.output_node.node_value == end_node.node_value:
                return out_edge
        return False

    def is_edge_between_nodes(self, start_node_value, end_node_value):
        start_node = self.get_node_by_value(start_node_value)
        end_node = self.get_node_by_value(end_node_value)
        # TODO :: handle node NOT FOUND
        return self.is_edge_between_nodes()

    def is_edge_between_nodes(self, start_node: Node, end_node: Node):
        is_edge_between = False
        for out_edge in start_node.output_edges:
            if out_edge.output_node.node_value == end_node.node_value:
                is_edge_between = True
                break
        return is_edge_between

    def is_edge_between_all_nodes(self):
        is_edge_between_all_nodes = True
        for i, node in enumerate(self.nodes):
            if i >= len(self.nodes) - 1:
                break
            if len(node.output_edges) < len(self.nodes) - 1:
                return False
        return is_edge_between_all_nodes

    def find_nodes_order_by_output_degree_desc(self):
        return sorted(self.nodes, key=lambda n: n.get_out_degree_including_edges_values(), reverse=True)

    def find_nodes_order_by_input_degree_desc(self):
        return sorted(self.nodes, key=lambda n: n.get_in_degree_including_edges_values(), reverse=True)

    def get_edges_order_by_edge_value_desc(self):
        return sorted(self.edges, key=lambda e: e.edge_value, reverse=True)

    def get_edges_by_exists_reverse_edge(self, exists_reverse_edge):
        filtered_edges = filter(lambda edge: edge.exists_reverse_edge == exists_reverse_edge, self.edges)
        return list(filtered_edges)

    def get_nodes_without_edges(self):
        filtered_nodes = filter(lambda node: len(node.input_edges) == 0 and len(node.output_edges) == 0,  self.nodes)
        return list(filtered_nodes)

    def get_nodes_with_edge_to_self(self):
        filtered_edges = filter(lambda edge: edge.input_node == edge.output_node, self.edges)
        edges_list = list(filtered_edges)
        nodes = []
        for edge in edges_list:
            nodes.append(edge.input_node)
        return nodes

    def print_n_nodes_highest_out_degree(self, n):
        nodes = self.find_nodes_order_by_output_degree_desc()
        for i in range(n):
            print(str(nodes[i].node_value) + " (" + str(nodes[i].get_out_degree_including_edges_values()) + ")")

    def print_edges_by_edge_value_gte(self, edge_value):
        edges = self.get_edges_order_by_edge_value_desc()
        for edge in edges:
            if edge.edge_value >= edge_value:
                print(str(edge.input_node.node_value) + " -> " + str(edge.output_node.node_value))
            else:
                break

    def print_node_values(self, nodes):
        result = ""
        for node in nodes:
            result += node.node_value + ", "
        print(result[:-2])

    def print_nodes_without_edges(self):
        nodes = self.get_nodes_without_edges()
        self.print_node_values(nodes)

    def print_nodes_with_edge_to_self(self):
        nodes = self.get_nodes_with_edge_to_self()
        self.print_node_values(nodes)

    def print_is_edge_between_all_nodes(self, prefix):
        print(prefix + str(self.is_edge_between_all_nodes()))

    def print_edges_by_exists_reverse_edge(self, exists_reverse_edge):
        edges = self.get_edges_by_exists_reverse_edge(exists_reverse_edge)
        for edge in edges:
            print(str(edge.output_node.node_value) + " -> " + str(edge.input_node.node_value))

    def print_node_max_output_degree(self, prefix):
        nodes = self.find_nodes_order_by_output_degree_desc()
        node = nodes[0]
        print(prefix + node.node_value + " (" + str(node.get_out_degree_including_edges_values()) + ")")

    def print_node_max_input_degree(self, prefix):
        nodes = self.find_nodes_order_by_input_degree_desc()
        node = nodes[0]
        print(prefix + node.node_value + " (" + str(node.get_in_degree_including_edges_values()) + ")")

    def print_edges_diff_case_node_values(self, connector):
        for edge in self.edges:
            edge.print_node_values_diff_case(connector)

