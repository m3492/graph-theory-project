class Edge:

    def __init__(self, input_node, output_node):
        self.edge_id = 0
        self.edge_value = 0
        self.input_node = input_node
        self.output_node = output_node
        self.exists_reverse_edge = False

    def __init__(self, edge_id, input_node, output_node):
        self.edge_id = edge_id
        self.edge_value = 0
        self.input_node = input_node
        self.output_node = output_node
        self.exists_reverse_edge = False

    def __init__(self, edge_id, edge_value, input_node, output_node):
        self.edge_id = edge_id
        self.edge_value = edge_value
        self.input_node = input_node
        self.output_node = output_node
        self.exists_reverse_edge = False

    def __repr__(self):
        return str(self.input_node.node_value) + " -> " + str(self.output_node.node_value) +\
               " " + str(self.edge_value)

    def print_node_values_diff_case(self, connector):
        if self.input_node.node_value.islower() and self.output_node.node_value.isupper():
            print(self.input_node.node_value + connector + self.output_node.node_value.lower())
        else:
            print(self.input_node.node_value + connector + self.output_node.node_value)




