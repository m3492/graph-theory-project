from graph import Graph

graph = Graph()
graph.node_values_case_sensitivity = False
graph.num_of_nodes_rows = 2
graph.create_elements_by_std_input()
graph.print_edges_diff_case_node_values(" -> ")
